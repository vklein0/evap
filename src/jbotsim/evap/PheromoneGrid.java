package jbotsim.evap;

import java.awt.*;

/**
 * Created by vklein on 06/03/15.
 * Grille composée de cellules couvrant la zone d'opération une cellule est la surface
 * minimale pouvant être couverte par une pheromone.
 */
public class PheromoneGrid implements Cloneable {

    static final float DEFAULT_EVAPORATION = (float) 0.001 ;
    private static final int DEFAULT_CELL_SIZE = 30;
    private static float evaporation = DEFAULT_EVAPORATION;


    //[columns][lines]
    private PheromoneCell[][] grid = null;
    private int cellSize = DEFAULT_CELL_SIZE;
    private int nbRows = 0;
    private int nbColumns = 0;

    /**
     * @param cellSize
     * @param width
     * @param height
     */
    public PheromoneGrid(int cellSize, int width, int height) {
        this.nbColumns  = (int) Math.ceil((float) width/cellSize);
        this.nbRows     = (int) Math.ceil((float) height/cellSize);
        this.grid       = new PheromoneCell[nbColumns][nbRows];
        this.cellSize   = cellSize;

        //init cells
        for(int i=0; i < nbRows; i++) {
            for(int j=0; j < nbColumns; j++){
                this.grid[j][i] = new PheromoneCell(j,i);
                //Compute the center of cell
                int x = j * cellSize + (cellSize/2);
                int y = i * cellSize + (cellSize/2);
                Point p = new Point(x,y);
                this.grid[j][i].setCenter(p);
            }
        }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        PheromoneGrid pG = (PheromoneGrid) super.clone();
        pG.grid =  new PheromoneCell[this.nbColumns][this.nbRows];

        for(int i=0; i < nbRows; i++) {
            for(int j=0; j < nbColumns; j++) {
                pG.grid[j][i] = (PheromoneCell) this.grid[j][i].clone();
            }
        }
        return pG;
    }

    public int getNbRows() {
        return nbRows;
    }

    public void setNbRows(int nbRows) {
        this.nbRows = nbRows;
    }

    public int getCellSize() {
        return cellSize;
    }

    public void setCellSize(int cellSize) {
        this.cellSize = cellSize;
    }

    public int getNbColumns() {
        return nbColumns;
    }

    public void setNbColumns(int nbColumns) {
        this.nbColumns = nbColumns;
    }

    public PheromoneCell[][] getGrid() {
        return grid;
    }

    public PheromoneCell getCell(int x, int y) {
        return grid[x][y];
    }

    public void evaporate() {
        for(int i=0; i < nbRows; i++) {
            for(int j=0; j < nbColumns; j++) {
                this.grid[j][i].evaporatePheromone(evaporation);
            }
        }
    }

    public static float getEvaporation() {
        return evaporation;
    }

    public static void setEvaporation(float evaporation) {
        PheromoneGrid.evaporation = evaporation;
    }
}
