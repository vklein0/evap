package jbotsim.evap.node;

/**
 * Created by vklein on 06/03/15.
 */
public class RedHatGnome extends GardenGnome {
    public RedHatGnome() {
        super();
        setProperty("icon", "/jbotsim/evap/resources/gnome-redhat.png");
        setSize(17);
    }
}