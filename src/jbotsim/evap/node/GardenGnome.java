package jbotsim.evap.node;

import jbotsim.Node;

/**
 * Created by vklein on 05/03/15.
 */
public class GardenGnome extends Node {
    public GardenGnome() {
        setProperty("size", 15);
        disableWireless();
    }
}
