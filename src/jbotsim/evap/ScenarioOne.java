package jbotsim.evap;

import jbotsim.Topology;
import jbotsim.evap.node.ControlStation;
import jbotsim.evap.node.RedHatGnome;
import jbotsim.evap.node.Walker;
import jbotsim.evap.ui.JEvapTopology;
import jbotsim.ui.JViewer;

/**
 * Created by vklein on 06/03/15.
 */
public class ScenarioOne {
    public static void main(String [] argv) {

        Topology topology = new Topology(1200, 800);
        topology.setNodeModel("walker", Walker.class);
        topology.setNodeModel("gnome", RedHatGnome.class);
        topology.setNodeModel("ControlStation", ControlStation.class);
        EvapEnvironment env = new EvapEnvironment(60, topology, (float)0.0005);
        env.setColoredPheromone(true);
        topology.setClockSpeed(40);

        JEvapTopology jEvapTopology = new JEvapTopology(env);
        jEvapTopology.setDisplayPheromoneAsNumeric(false);

        new JViewer(jEvapTopology);
    }
}
