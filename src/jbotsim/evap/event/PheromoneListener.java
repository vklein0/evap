package jbotsim.evap.event;

import jbotsim.evap.PheromoneCell;
import java.util.EventListener;

/**
 * Created by vklein on 10/03/15.
 */
public interface PheromoneListener extends EventListener {
    void onPheromoneDrop(PheromoneCell cell, float phQuantity);
}
