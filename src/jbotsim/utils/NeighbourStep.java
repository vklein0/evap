package jbotsim.utils;

/**
 * [ N:(x,y+1), NE:(x+1, y+1), E:(x+1, y), SE:(x+1, y-1), S:(x, y-1), SW:(x-1, y-1), W:(x-1, y), NW:(x-1, y+1)]
 * Created by vklein on 13/04/15.
 */
public enum NeighbourStep {
    NORTH(0,-1),
    NORTHEAST(1,-1),
    EAST(1,0),
    SOUTHEAST(1,1),
    SOUTH(0,1),
    SOUTHWEST(-1,1),
    WEST(-1,0),
    NORTHWEST(-1,-1);

    private int xstep;
    private int ystep;

    NeighbourStep(int x, int y) {
        this.xstep = x;
        this.ystep = y;
    }

    public int getXstep() {
        return xstep;
    }

    public int getYstep() {
        return ystep;
    }
} // end enum